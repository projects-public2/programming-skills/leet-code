from collections import defaultdict
import time
from typing import List


class Solution:
    def groupAnagrams1(self, strs: List[str]) -> List[List[str]]:
        result_dict = dict[list]()
        for item in strs:
            item_sort = "".join(sorted(item))
            if not result_dict.get(item_sort):
                result_dict[item_sort] = [item]
            else:
                result_dict[item_sort].append(item)
        
        return result_dict.values()
    
    def groupAnagrams2(self, strs: List[str]) -> List[List[str]]:       # 2 cách giống nhau, đều chung 1 ý tưởng chỉ khác cách viết
        result = defaultdict(list)
        for item in strs:
            item_sort = "".join(sorted(item))
            result[item_sort].append(item)
        
        return result.values()
    
    
if __name__ == "__main__":
    print("Start")
    data = ["eat","tea","tan","ate","nat","bat"]
    
    t1 = time.time()
    print("Result list data anagram groups: ", Solution().groupAnagrams2(data))
    t2 = time.time()
    print('Elapsed time 1: {} seconds'.format((t2 - t1)))
    
    