from collections import Counter
import time


class Solution:
    def isAnagram1(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False
        
        if len(s) == 1 or len(t) == 1:
            return (s == t)
        
        value1 = [item for item in s]
        value2 = [item for item in t]
        value1.sort()
        value2.sort()
                
        for index in range(0, len(value1) - 1):
            if (value1[index] != value2[index]):
                return False
        return True
    
    def isAnagram2(self, s: str, t: str) -> bool:
        couter_s = Counter(s)
        couter_t = Counter(t)
        
        return couter_s == couter_t
    

if __name__ == "__main__":
    s = "anagram"
    t = "nagaram"
        
    t1 = time.time()
    print(Solution().isAnagram1(s, t))
    t2 = time.time()
    print('Elapsed time 1: {} seconds'.format((t2 - t1)))
    
    # t1 = time.time()
    # print(Solution().isAnagram2(s, t))
    # t2 = time.time()
    # print('Elapsed time 1: {} seconds'.format((t2 - t1)))