import time
from ast import literal_eval
from numpy import random
from typing import List


class Solution:
    def containsDuplicate1(self, nums: List[int]) -> bool:          # O(nlogn)
        nums.sort()                                 # O(nlogn)
        for index in range(0, len(nums) - 2):       # O(n - 1) => O(n)
            if nums[index] == nums[index + 1]:      # O(1)
                return True        
        return False
        
    def containsDuplicate2(self, nums: List[int]) -> bool:          # O(n)
        nums_set = set(nums)                        # O(n)
        if len(nums_set) != len(nums):              # O(1)
            return True
        return False
    
    def containsDuplicate3(self, nums: List[int]) -> bool:          # O(n)
        nums_set = set()                        
        for num in nums:                     # O(n)
            if num in nums_set:              # O(1)
                return True
            else:
                nums_set.add(num)
        return False


if __name__ == "__main__":
    nums = []
    with open("data - 217. Contains Duplicate.txt", 'r') as f:
        nums = literal_eval(f.read())

    t1 = time.time()
    print(Solution().containsDuplicate1(nums=nums))
    t2 = time.time()
    print('Elapsed time 1: {} seconds'.format((t2 - t1)))



    t1 = time.time()
    print(Solution().containsDuplicate2(nums=nums))             
    t2 = time.time()
    print('Elapsed time 2: {} seconds'.format((t2 - t1)))
    


    t1 = time.time()
    print(Solution().containsDuplicate3(nums=nums))             
    t2 = time.time()
    print('Elapsed time 3: {} seconds'.format((t2 - t1)))
    
    # function 1: 0.018649816513061523 seconds
    # function 2: 0.008685588836669922 seconds
    # => function 2 nhanh hơn function 1 gấp 10 lần
    